/* CLIENT */

#define _OPEN_SYS
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <stdlib.h>

#define BUFF_SIZE 512
#define CLNTS 20
#define DEBUG_MODE 1

void menu_inicial(); 

struct t_buffer {
  char buff[BUFF_SIZE];
  int pos;
};

struct t_msg {
  unsigned char cid, type, size, chk;
  unsigned char msg[256];
};

struct j_barco_plan {
	unsigned char tamano, cantidad;
};

struct j_barco {
	unsigned char xi, xf, yi, yf;
};

struct t_juego {
	unsigned char jid, size;
	int barcos_num, bar_cant;
	char nombre[255];
	struct j_barco_plan barcos[255];
	struct j_barco mis_barcos[255];
	unsigned char tablero[255][255];
};

int rfd, wfd;
char cid;
char fnin[20];
struct t_buffer rbuff;
struct t_juego juego, oponente;

int main() {
	rbuff.pos = 0;
	printf("Bienvenido al asombroso juego de Battleship!\n");
	menu_inicial();
	exit(0);
}

/* CAPA DE COMUNICACION */

void concat(char *arr1, int size1, char *arr2, int size2, char *result, int printable) {
	
	int i = 0;
	
	while (i < size1) {
		if (*arr1 == 0 && printable == 1) break;
		*result = *arr1;
		result++;
		arr1++;
		i++;
	}
	
	i = 0;
	while (i < size2) {
		*result = *arr2;
		result++;
		arr2++;
		i++;
	}
}

int parse_bytestoint(unsigned char *input, int offset) {
	
	int r = ((unsigned char) *(input + offset)     << 24) |
	        ((unsigned char) *(input + offset + 1) << 16) |
	        ((unsigned char) *(input + offset + 2) << 8 ) |
	         (unsigned char) *(input + offset + 3);
	         
	return r;
}

void parse_inttobytes(int input, unsigned char *value) {
	
	int i = 3;
	
	while (i >= 0) {
		*value = (input >> i * 8) & 0xff;
		value++;
		i--;
	}
}

int checksum(struct t_msg msg) {
	if (msg.chk == 0x37)
		return 0;
	else return -1;
}

char calc_checksum(struct t_msg msg) {
	return 0x37;
}

int push_buff(struct t_buffer *buffer, unsigned char in) {
	
	if ((*buffer).pos < sizeof((*buffer).buff)) {
		
		(*buffer).buff[(*buffer).pos] = in;
		(*buffer).pos++;
		return 0;
	}
	else return -1;
}

int pull_buff(struct t_buffer *buffer, unsigned char *out) {
	
	if ((*buffer).pos > 0) {
		
		*out = (*buffer).buff[0];
		int i = 0;
		while (i < (*buffer).pos - 1) {
			(*buffer).buff[i] = (*buffer).buff[i + 1];
			i++;
		}
		(*buffer).pos--;
		return 0;
	}
	else return -1;
}

void sprint_msg(struct t_msg msg, char *str) {
	
	char s1[10];
	
	sprintf((char *)str, "%02x %02x %02x ", 
		(unsigned char) msg.cid, 
		(unsigned char) msg.type, 
		(unsigned char) msg.size);
		
	int i = 0;
	while (i < msg.size) {
		sprintf(s1, "%02x ", (unsigned char) msg.msg[i]);
		concat((char *)str, BUFF_SIZE, &s1[0], 10, (char *)str, 1);
		i++;
	}
	sprintf(s1, "%02x", (unsigned char) msg.chk);
	concat(str, BUFF_SIZE, &s1[0], 3, str, 1);
}

int read_package(int read_end, struct t_buffer *buffer) {
	
	char in[BUFF_SIZE];
	
	int nb;
	nb = read(read_end, in, sizeof(in));
	
	if (nb >= 0) {
		
		int i = 0;
		while (i < nb) {
			
			if (push_buff(buffer, in[i]) < 0) break;
			i++;
		}
		return 0;
	}
	else return -1;
}

int pre_read_msg(int read_end, struct t_buffer *buffer, struct t_msg *msg) {
	
	if (pull_buff(buffer, &(*msg).cid) < 0) return -1;
	if (pull_buff(buffer, &(*msg).type) < 0) return -1;
	if (pull_buff(buffer, &(*msg).size) < 0) return -1;
	
	int i = 0;
	while (i < (*msg).size) {
		if (pull_buff(buffer, &(*msg).msg[i]) < 0) return -1;
		i++;
	}
	
	if (pull_buff(buffer, &(*msg).chk) < 0) return -1;
	
	if (checksum(*msg) < 0) return -2;
	
	return 0;
}

void read_msg(int read_end, struct t_buffer *buffer, struct t_msg *message) {
	while(1) {
		int pr = pre_read_msg(read_end, buffer, message);
		
		if (pr < 0) {
			if (pr == -1) {
				read_package(read_end, buffer);
			}
			else if (pr == -2) {
				printf("Se ha corrompido algun mensaje entrante. (Checksum err)\n");
				unsigned char a;
				pull_buff(buffer, &a);
			}
		}
		else break;
	}
	
	if (DEBUG_MODE > 0) {
		char mes [BUFF_SIZE];
		sprint_msg(*message, &mes[0]);
		printf("  $debug msg read: %s\n", mes);
	}
}

void send_msg(int write_end, struct t_msg msg) {
	
	char m[msg.size + 4];
	
	m[0] = msg.cid;
	m[1] = msg.type;
	m[2] = msg.size;
	
	int i = 0;
	while (i < msg.size) {
		m[i + 3] = msg.msg[i];
		i++;
	}
	m[i + 3] = calc_checksum(msg);
	
	write(write_end, &m, sizeof(m)); 
	
	if (DEBUG_MODE > 0) {
		char mes [BUFF_SIZE];
		sprint_msg(msg, &mes[0]);
		printf("  $debug msg sent: %s\n", mes);
	}
} 

/* PROTOCOLO */

int request_state(int *expectador, int *jugador) {
	
	struct t_msg rs;
	rs.cid = cid;
	rs.type = 0xf2;
	rs.size = 0;
	send_msg(wfd, rs);
	
	struct t_msg ss;
	read_msg(rfd, &rbuff, &ss);
	
	if(ss.type != 0xf3)
		return -1;
		
	*expectador = ss.msg[0];
	*jugador = ss.msg[1];
	
	return 0;
}

int send_create_game_req(int size, char *nombre, int nombre_size, unsigned char *barcos, int barcos_size) {
	
	struct t_msg cgr;
	cgr.cid = cid;
	cgr.type = 0xfa;
	cgr.size = nombre_size + barcos_size + 2;
	cgr.msg[0] = (unsigned char) size;
	
	unsigned char *barcbck = barcos;
	
	int i = 0;
	while (i < nombre_size) {
		cgr.msg[i + 1] = *nombre;
		nombre++;
		i++;
	}
	cgr.msg[i + 1] = 0;
	i++;
	while (i < barcos_size + nombre_size + 1) {
		cgr.msg[i + 1] = *barcos;
		barcos++;
		i++;
	}
	
	send_msg(wfd, cgr);
	
	struct t_msg ccg;
	read_msg(rfd, &rbuff, &ccg);
	
	if(ccg.type != 0xfb)
		return -1;
	
	juego.jid = ccg.msg[0];
	juego.size = size;
	i = 0;
	int j = 0;
	while (i < barcos_size) {
		struct j_barco_plan b;
		b.tamano = *(barcbck++);
		b.cantidad = *(barcbck++);
		i += 2;
		juego.barcos[j++] = b;
	}
	juego.barcos_num = j;
	juego.bar_cant = 0;
	
	return 0;
}

int request_play_list(struct t_juego *juegos, int *no_juegos) {
	
	struct t_msg rpl;
	rpl.cid = cid;
	rpl.type = 0xf4;
	rpl.size = 0;
	send_msg(wfd, rpl);
	
	struct t_msg pl;
	read_msg(rfd, &rbuff, &pl);
	
	if(pl.type != 0xf5)
		return -1;
		
	int i = 0;
	int j = 0;
	while(i < pl.size) {
		struct t_juego juego;
		juego.jid = pl.msg[i++];
		juego.size = pl.msg[i++];
		char *p = &juego.nombre[0];
		while(i < pl.size) {
			*p = pl.msg[i++];
			if(*p == 0)
				break;
			p++;
		}
		juegos[j++] = juego;
	}
	*no_juegos = j;
	
	return 0;
}

int req_play_game(unsigned char jid) {
	
	struct t_msg rpg;
	rpg.cid = cid;
	rpg.type = 0xf8;
	rpg.size = 1;
	rpg.msg[0] = jid;
	
	send_msg(wfd, rpg);
	
	struct t_msg cpg;
	read_msg(rfd, &rbuff, &cpg);
	
	if (cpg.size == 0)
		return -1;
		
	juego.size = cpg.msg[0];
	int i = 1;
	int j = 0;
	while (i < cpg.size) {
		struct j_barco_plan b;
		b.tamano = cpg.msg[i++];
		b.cantidad = cpg.msg[i++];
		juego.barcos[j++] = b;
	}
	juego.barcos_num = j;
	juego.bar_cant = 0;
	
	return 0;
}

/* CAPA DE EN MEDIO */

int perform_hs() {
	
	pid_t pid;
	pid = getpid();
	// printf("Porcess id: %d (0x%x)\n", pid, pid);
	
	char fnout[20];
	int rfdw, fifo_wendy;
	sprintf(fnin, "/tmp/i_%d", pid);
	sprintf(fnout, "/tmp/o_%d", pid);
	
	if (mkfifo(fnin, S_IRWXU) != 0)
		perror("create input fifo");
	if((fifo_wendy = open("/tmp/wendy",O_RDWR)) < 0)
		perror("open wendy's end");
	
	struct t_msg hs;
	hs.cid = 0x00;
	hs.type = 0xf0;
	hs.size = 4;
	parse_inttobytes(pid, &hs.msg[0]);
	
	send_msg(fifo_wendy, hs);
	
	char mes [BUFF_SIZE];
	sprint_msg(hs, &mes[0]);
	// printf("  Msg sent to wendy: %s\n", mes);
	
	if ((rfd = open(fnin, O_RDONLY)) < 0)
		perror("Abrir read-end i_");
	if ((rfdw = open(fnin, O_WRONLY)) < 0)
		perror("Abrir write-end i_");
	
	struct t_buffer buff_in;
	buff_in.pos = 0;
	
	struct t_msg hsr;
	read_msg(rfd, &buff_in, &hsr);
	
	sprint_msg(hsr, &mes[0]);
	// printf("mensaje recibido: %s\n", mes);
	
	cid = hsr.msg[0];
	
	printf("  Handshake with server was successfull\n");
	// printf("  Client id: %02x\n", (unsigned char) cid);
	
	if ((wfd = open(fnout, O_WRONLY)) < 0)
		perror("Abrir write-end client o_");
	
	close(fifo_wendy);
	
	close(rfdw);
	
	return 0;
}

int wait_for_mate() {
	printf("  Waiting for a mate to play with...\n");
	
	struct t_msg sg;
	read_msg(rfd, &rbuff, &sg);
	
	if(sg.type != 0x01)
		return -1;
	
	return 0;
}

/* CAPA DE APLICACION */

void print_game(struct t_juego juego) {
	
	int c = 0;
	printf("  ");
	while (c++ < juego.size) {
		printf("%2c", 'A' + c - 1);
	}
	printf("\n");
	c = 0;
	while (c < juego.size) {
		int f = 0;
		printf("%2d", c + 1);
		while (f < juego.size) {
			printf("%2c", juego.tablero[c][f++]);
		}
		c++;
		printf("\n");
	}
	printf("\n");
}

void take_turn(struct t_msg t) {
	
	if (t.type != 0x03)
		return;
		
	if (t.size != 0) {
		printf("El oponente jugo su turno.\n");
		printf("Fuiste atacado en la casilla %c%d.\n",t.msg[0]+'A',t.msg[1]);
		if (t.msg[2] > 0) {
			printf("Te han golpeado en un barco.\n");
		}
		else {
			printf("Su ataque fue errado. Estas a salvo.\n");
		}
		printf("Este es el mapa de tus barcos despues de su turno: \n");
	}
	else {
		printf("Todo listo para comenzar el juego!\n");
		printf("Este es el mapa de tus barcos antes de comenzar: \n");
	}
	printf("\n");
	
	print_game(juego);
	
	printf("Para realizar tu ataque presiona enter.\n");
	getchar();
	
	system("clear");
	printf("Este es el mapa conocido de tu oponente:\n");
	printf("\n");
	
	print_game(oponente);
	
}

void init_tablero() {
	
	int c = 0;
	while (c < juego.size) {
		int f = 0;
		while (f < juego.size) {
			juego.tablero[c][f++] = '~';
		}
		c++;
	}
	oponente.size = juego.size;
	c = 0;
	while (c < oponente.size) {
		int f = 0;
		while (f < oponente.size) {
			oponente.tablero[c][f++] = '~';
		}
		c++;
	}
	
	int b = -1;
	while (b++ < juego.bar_cant) {
		char dir = 'h';
		struct j_barco bar;
		bar = juego.mis_barcos[b];
		int tamano = bar.xf - bar.xi;
		if(tamano == 0){
			tamano = bar.yf - bar.yi;
			dir = 'v';
		}
		int i = 0;
		int x = bar.xi;
		int y = bar.yi;
		tamano++;
		while (i++ < tamano) {
			juego.tablero[y][x] = '#';
			if(dir=='h')
				x++;
			else
				y++;
		}
	}
}

void place_ships() {
	printf("Start placing your ships: \n");
	
	struct t_msg cb;
	cb.cid = cid;
	cb.type = 0x02;
	
	int msgi = 0;
	
	int t = 0;
	int b = 0;
	while (t < juego.barcos_num) {
		struct j_barco_plan tipo;
		tipo = juego.barcos[t++];
		int i = 0;
		while(i++ < tipo.cantidad) {
			printf("Barco %d:\n", ++b);
			printf("  Tamano: %d\n", tipo.tamano);
			printf("  Orientacion (v/h): ");
			char in[10], xin[10];
			scanf("%s", in);
			printf("  Columna (A-%c): ", 'A' + juego.size - 1);
			int yin = 0;
			int *yinint = &yin;
			scanf("%s", xin);
			printf("  Fila (1-%d): ", juego.size);
			scanf("%d", yinint);
			printf("\n");
			
			struct j_barco bar;
			bar.xi = xin[0] - 'A';
			bar.yi = yin - 1;
			if(in[0] == 'v') {
				bar.xf = bar.xi;
				bar.yf = bar.yi + tipo.tamano - 1;
			}
			else {
				bar.xf = bar.xi + tipo.tamano - 1;
				bar.yf = bar.yi;
			}
			
			cb.msg[msgi++] = bar.xi;
			cb.msg[msgi++] = bar.yi;
			cb.msg[msgi++] = bar.xf;
			cb.msg[msgi++] = bar.yf;
			
			juego.mis_barcos[juego.bar_cant++] = bar;
		}
	}
	juego.bar_cant--;
	
	cb.size = msgi;
	
	send_msg(wfd, cb);
}

void playgame() {
	system("clear");
	printf("Everything ready to play :)\n");
	place_ships();
	init_tablero();
	print_game(juego);
	
	struct t_msg turno;
	while(1) {
		printf("Esperando tu turno...\n");
		
		read_msg(rfd, &rbuff, &turno);
		
		if (turno.type == 0x06)
			break;
			
		system("clear");
		take_turn(turno);
	}
	//sleep(10);
}

void select_game_to_play() {
	
	struct t_juego juegos[CLNTS];
	int num = 0;
	request_play_list(&juegos[0], &num);
	
	printf("  Juegos disponibles (%d):\n", num);
	int i = 0;
	while(i < num) {
		printf("  %d. [%02x] %s (%d x %d)\n", i+1, juegos[i].jid, 
			juegos[i].nombre, juegos[i].size, juegos[i].size);
		i++;
	}
	
	char in[10];
	printf("  Seleccione uno de los anteriores para jugar: ");
	scanf("%s", in);
	unsigned char op = in[0] - '0';
	
 	int res = req_play_game(juegos[op - 1].jid);
	
	if (res >= 0) {
		playgame();
	}
}

void create_game_menu() {
	
	char in[100];
	int def = 10;
	int *intin = &def;
	printf("  Ingrese el tamano del tablero: ");
	scanf("%d", intin);
	
	int nombre_size = 0;
	printf("  Ingrese el nombre del juego: ");
	scanf("%s", in);
	char *p = &in[0];
	while (nombre_size < BUFF_SIZE) {
		if(*p == 0)
			break;
		p++;
		nombre_size++;
	}
	
	unsigned char barcos[BUFF_SIZE];
	int tipos = 0;
	int barc = 0;
	int nbar = 0;
	while(1) {
		int *bin = &barc;
		int *nba = &nbar;
		printf("  Ingrese el tamano de barcos que quiere colocar");
		if (tipos > 0)
			printf("\n    (o ingrese 0 (cero) para terminar de ingresar barcos)");
		printf(": ");
		
		scanf("%d", bin);
		
		if(*bin <= 0) {
			if (tipos > 0)
				break;
			continue;
		}
		
		printf("  Cuantos barcos de tamano %d quiere ingresar?: ", *bin);
		scanf("%d", nba);
		
		barcos[tipos] = (unsigned char) *bin;
		barcos[tipos + 1] = (unsigned char) *nba;
		
		tipos += 2;
	}
	
	if (send_create_game_req(*intin, &in[0], nombre_size, &barcos[0], tipos) >= 0) {
		printf("  Juego creado con exito!\n");
		int cg = wait_for_mate();
		if(cg >= 0)
			playgame();
	}
	
}

int game_menus() {
	
	int exp, jug;
	while (request_state(&exp, &jug) < 0) ;
	
	while(1) {
		printf("\n");
		if(exp > 0 && jug > 0)
			printf("  Hay juego(s) disponibles para ser expectador y para jugar.\n");
		else if(exp > 0)
			printf("  Hay juego(s) disponibles para ser expectador.\n");
		else if(jug > 0)
			printf("  Hay juego(s) disponibles para jugar.\n");
		else
			printf("  No hay juegos abiertos.\n");
		printf("  Seleccione una opcion:\n");
		printf("  1. Crear un nuevo juego\n");
		if(jug > 0)
			printf("  2. Seleccionar un juego para jugar\n");
		if(exp > 0)
			printf("  3. Seleccionar un juego para ver\n");
		printf("  0. Desconectarse del servidor\n");
		printf("  > ");
		
		char in[10];
		scanf("%s", in);
		
		switch(in[0]){
			case '0':
			return -1;
			
			case '1':
			create_game_menu();
			return 1;
			
			case '2':
			select_game_to_play();
			return 1;
			
			case '3':
			printf("Esta opcion aun no ha sido implementada...\n");
			
			default:
			printf("Seleccione una opcion valida...\n");
			break;
		}
	}
	return 0;
}

void start_clnt() {
	
	if (perform_hs() < 0)
		perror("Handshake");
		
	while(game_menus() > 0) ;
	
	// mensaje que no es del protocolo para salir
	struct t_msg ext;
	ext.cid = cid;
	ext.type = 0xee;
	ext.size = 0;
	send_msg(wfd, ext);
	
	printf("  Disconnected from server.\n");
	
	close(rfd);
	close(wfd);
	unlink(fnin);
}

void menu_inicial() {
	
	int cont = 0;
	
	while(cont == 0) {
		
		printf("\n");
		printf("Selecciona una opcion:\n");
		printf("  1. Conectar con el servidor\n");
		printf("  0. Salir\n"); 
		printf("> ");
	
		char in[10];
		scanf("%s", in);
		
		switch(in[0]){
			case '1':
			start_clnt();
			break;
			
			case '0':
			cont = -1;
			break;
			
			default:
			printf("Seleccione una opcion valida...\n");
			break;
		}
	}
}
