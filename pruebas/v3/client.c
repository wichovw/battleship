/* CLIENT */

#define _OPEN_SYS
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <stdlib.h>

#define BUFF_SIZE 512

void pruebas_unitarias();

struct t_buffer {
  char buff[BUFF_SIZE];
  int pos;
};

struct t_msg {
  char cid, type, size, chk;
  char msg[256];
};

int main() {
	pruebas_unitarias();
	exit(0);
}

void concat(char *arr1, int size1, char *arr2, int size2, char *result, int printable) {
	
	int i = 0;
	
	while (i < size1) {
		if (*arr1 == 0 && printable == 1) break;
		*result = *arr1;
		result++;
		arr1++;
		i++;
	}
	
	i = 0;
	while (i < size2) {
		*result = *arr2;
		result++;
		arr2++;
		i++;
	}
}

int parse_bytestoint(char *input, int offset) {
	
	int r = ((unsigned char) *(input + offset)     << 24) |
	        ((unsigned char) *(input + offset + 1) << 16) |
	        ((unsigned char) *(input + offset + 2) << 8 ) |
	         (unsigned char) *(input + offset + 3);
	         
	return r;
}

void parse_inttobytes(int input, char *value) {
	
	int i = 3;
	
	while (i >= 0) {
		*value = (input >> i * 8) & 0xff;
		value++;
		i--;
	}
}

int checksum(struct t_msg msg) {
	if (msg.chk == 0x37)
		return 0;
	else return -1;
}

char calc_checksum(struct t_msg msg) {
	return 0x37;
}

int push_buff(struct t_buffer *buffer, char in) {
	
	if ((*buffer).pos < sizeof((*buffer).buff)) {
		
		(*buffer).buff[(*buffer).pos] = in;
		(*buffer).pos++;
		return 0;
	}
	else return -1;
}

int pull_buff(struct t_buffer *buffer, char *out) {
	
	if ((*buffer).pos > 0) {
		
		*out = (*buffer).buff[0];
		int i = 0;
		while (i < (*buffer).pos - 1) {
			(*buffer).buff[i] = (*buffer).buff[i + 1];
			i++;
		}
		(*buffer).pos--;
		return 0;
	}
	else return -1;
}

void sprint_msg(struct t_msg msg, char *str) {
	
	char s1[10];
	
	sprintf((char *)str, "%02x %02x %02x ", 
		(unsigned char) msg.cid, 
		(unsigned char) msg.type, 
		(unsigned char) msg.size);
		
	int i = 0;
	while (i < msg.size) {
		sprintf(s1, "%02x ", (unsigned char) msg.msg[i]);
		concat((char *)str, BUFF_SIZE, &s1[0], 10, (char *)str, 1);
		i++;
	}
	sprintf(s1, "%02x", (unsigned char) msg.chk);
	concat(str, BUFF_SIZE, &s1[0], 3, str, 1);
}

int read_package(int read_end, struct t_buffer *buffer) {
	
	char in[BUFF_SIZE];
	
	int nb;
	nb = read(read_end, in, sizeof(in));
	
	if (nb >= 0) {
		
		int i = 0;
		while (i < nb) {
			
			if (push_buff(buffer, in[i]) < 0) break;
			i++;
		}
		return 0;
	}
	else return -1;
}

int pre_read_msg(int read_end, struct t_buffer *buffer, struct t_msg *msg) {
	
	if (pull_buff(buffer, &(*msg).cid) < 0) return -1;
	if (pull_buff(buffer, &(*msg).type) < 0) return -1;
	if (pull_buff(buffer, &(*msg).size) < 0) return -1;
	
	int i = 0;
	while (i < (*msg).size) {
		if (pull_buff(buffer, &(*msg).msg[i]) < 0) return -1;
		i++;
	}
	
	if (pull_buff(buffer, &(*msg).chk) < 0) return -1;
	
	if (checksum(*msg) < 0) return -2;
	
	return 0;
}

void read_msg(int read_end, struct t_buffer *buffer, struct t_msg *message) {
	while(1) {
		int pr = pre_read_msg(read_end, buffer, message);
		
		if (pr < 0) {
			if (pr == -1) {
				read_package(read_end, buffer);
			}
			else if (pr == -2) {
				printf("Se ha corrompido algun mensaje entrante. (Checksum err)\n");
				char a;
				pull_buff(buffer, &a);
			}
		}
		else break;
	}
}

void send_msg(int write_end, struct t_msg msg) {
	
	char m[msg.size + 4];
	
	m[0] = msg.cid;
	m[1] = msg.type;
	m[2] = msg.size;
	
	int i = 0;
	while (i < msg.size) {
		m[i + 3] = msg.msg[i];
		i++;
	}
	m[i + 3] = calc_checksum(msg);
	
	write(write_end, &m, sizeof(m)); 
} 

void pruebas_unitarias() {
	
	pid_t pid;
	pid = getpid();
	printf("Porcess id: %d (0x%x)\n", pid, pid);
	
	char fn[20];
	int rfd, rfdw, fifo_wendy;
	sprintf(fn, "/tmp/i_%d", pid);
	
	if (mkfifo(fn, S_IRWXU) != 0)
		perror("create input fifo");
	
	if((fifo_wendy = open("/tmp/wendy",O_RDWR)) < 0)
		perror("open wendy's end");
	
	struct t_msg hs;
	hs.cid = 0x00;
	hs.type = 0xf0;
	hs.size = 4;
	parse_inttobytes(pid, &hs.msg[0]);
	
	send_msg(fifo_wendy, hs);
	
	printf("Message sent to wendy\n");
	
    if ((rfd = open(fn, O_RDONLY)) < 0)
		perror("Abrir read-end i_");
	if ((rfdw = open(fn, O_WRONLY)) < 0)
		perror("Abrir write-end i_");
		
	sleep(1);
	
	close(fifo_wendy);
	
	
	struct t_buffer buff_in;
	buff_in.pos = 0;
	
	struct t_msg hsr;
	read_msg(rfd, &buff_in, &hsr);
	
	char mes[BUFF_SIZE];
	sprint_msg(hsr, &mes[0]);
	printf("mensaje recibido: %s\n", mes);
	
	char cid = hsr.msg[0];
	
	printf("Handshake successfull\n");
	printf("Client id: %02x", (unsigned char) cid);
	
	close(rfd);
	close(rfdw);
	unlink(fn);
}
