/* SERVER */

#define _OPEN_SYS
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <stdlib.h>

#define CLNTS 20 // Numero maximo de clientes
#define BUFF_SIZE 512 // tamano de buffer de mensajes

void pruebas_unitarias();

struct t_client {
  int pid, rfd, wfd, i;
  char cid;
};

struct t_buffer {
  char buff[BUFF_SIZE];
  int pos;
};

struct t_msg {
  char cid, type, size, chk;
  char msg[256];
};

struct t_client clist[CLNTS];
char next_cid = 0xC0;
int active_clients = 0;

int main() {
	pruebas_unitarias();
	exit(0);
}

/* CAPA DE COMUNICACION */

void concat(char *arr1, int size1, char *arr2, int size2, char *result, int printable) {
	
	int i = 0;
	
	while (i < size1) {
		if (*arr1 == 0 && printable == 1) break;
		*result = *arr1;
		result++;
		arr1++;
		i++;
	}
	
	i = 0;
	while (i < size2) {
		*result = *arr2;
		result++;
		arr2++;
		i++;
	}
}

int push_buff(struct t_buffer *buffer, char in) {
	
	if ((*buffer).pos < sizeof((*buffer).buff)) {
		
		(*buffer).buff[(*buffer).pos] = in;
		(*buffer).pos++;
		return 0;
	}
	else return -1;
}

int pull_buff(struct t_buffer *buffer, char *out) {
	
	if ((*buffer).pos > 0) {
		
		*out = (*buffer).buff[0];
		int i = 0;
		while (i < (*buffer).pos - 1) {
			(*buffer).buff[i] = (*buffer).buff[i + 1];
			i++;
		}
		(*buffer).pos--;
		return 0;
	}
	else return -1;
}

int read_package(int read_end, struct t_buffer *buffer) {
	
	char in[BUFF_SIZE];
	
	int nb;
	nb = read(read_end, in, sizeof(in));
	
	//printf("Received package size: %d\n",nb);
	
	if (nb >= 0) {
		
		int i = 0;
		while (i < nb) {
			
			if (push_buff(buffer, in[i]) < 0) break;
			i++;
		}
		return 0;
	}
	else return -1;
}

void sprint_msg(struct t_msg msg, char *str) {
	
	char s1[10];
	
	sprintf((char *)str, "%02x %02x %02x ", 
		(unsigned char) msg.cid, 
		(unsigned char) msg.type, 
		(unsigned char) msg.size);
		
	int i = 0;
	while (i < msg.size) {
		sprintf(s1, "%02x ", (unsigned char) msg.msg[i]);
		concat((char *)str, BUFF_SIZE, &s1[0], 10, (char *)str, 1);
		i++;
	}
	sprintf(s1, "%02x", (unsigned char) msg.chk);
	concat(str, BUFF_SIZE, &s1[0], 3, str, 1);
}

int checksum(struct t_msg msg) {
	if (msg.chk == 0x37)
		return 0;
	else return -1;
}

char calc_checksum(struct t_msg msg) {
	return 0x37;
}

int pre_read_msg(int read_end, struct t_buffer *buffer, struct t_msg *msg) {
	
	if (pull_buff(buffer, &(*msg).cid) < 0) return -1;
	if (pull_buff(buffer, &(*msg).type) < 0) return -1;
	if (pull_buff(buffer, &(*msg).size) < 0) return -1;
	
	int i = 0;
	while (i < (*msg).size) {
		if (pull_buff(buffer, &(*msg).msg[i]) < 0) return -1;
		i++;
	}
	
	if (pull_buff(buffer, &(*msg).chk) < 0) return -1;
	
	if (checksum(*msg) < 0) return -2;
	
	return 0;
}

void read_msg(int read_end, struct t_buffer *buffer, struct t_msg *message) {
	while(1) {
		int pr = pre_read_msg(read_end, buffer, message);
		
		if (pr < 0) {
			if (pr == -1) {
				read_package(read_end, buffer);
			}
			else if (pr == -2) {
				printf("Se ha corrompido algun mensaje entrante. (Checksum err)\n");
				char a;
				pull_buff(buffer, &a);
			}
		}
		else break;
	}
}

int parse_bytestoint(char *input, int offset) {
	
	int r = ((unsigned char) *(input + offset)     << 24) |
	        ((unsigned char) *(input + offset + 1) << 16) |
	        ((unsigned char) *(input + offset + 2) << 8 ) |
	         (unsigned char) *(input + offset + 3);
	         
	return r;
}

void parse_inttobytes(int input, char *value) {
	
	int i = 3;
	
	while (i >= 0) {
		*value = (input >> i * 8) & 0xff;
		value++;
		i--;
	}
}

void send_msg(int write_end, struct t_msg msg) {
	
	char m[msg.size + 4];
	
	m[0] = msg.cid;
	m[1] = msg.type;
	m[2] = msg.size;
	
	int i = 0;
	while (i < msg.size) {
		m[i + 3] = msg.msg[i];
		i++;
	}
	m[i + 3] = calc_checksum(msg);
	
	write(write_end, &m, sizeof(m)); 
} 

struct t_client create_client(int pid) {
	
	// Se bloquea hasta que haya un espacio
	while(active_clients >= CLNTS) ;
	
	clist[active_clients].cid = next_cid;
	clist[active_clients].pid = pid;
	clist[active_clients].i = active_clients;
	
	active_clients++;
	next_cid++;
	
	return clist[active_clients - 1];
}

void free_client(int cindex) {
	
	while(cindex + 1 < CLNTS){
		
		clist[cindex] = clist[cindex + 1];
		clist[cindex].i--;
		cindex++;
	}
	
	active_clients--;
}

void pruebas_unitarias() {
	
	/* concat */
	
	char hola[] = "Hola ";
	char mundo[] = "Mundo!";
	
	int size = sizeof(hola) + sizeof(mundo);
	char cat[size];
	
	concat(&hola[0], sizeof(hola), &mundo[0], sizeof(mundo), &cat[0], 1);
	printf("concat %s%s: %s\n", hola, mundo, cat);
	
	/* push & pull buff */
	
	struct t_buffer prueba;
	prueba.pos = 0;
	
	char primero = 'p';
	char segundo = 's';
	char tercero = 't';
	
	push_buff(&prueba, primero);
	push_buff(&prueba, segundo);
	push_buff(&prueba, tercero);
	
	pull_buff(&prueba, &tercero);
	pull_buff(&prueba, &segundo);
	pull_buff(&prueba, &primero);
	
	printf("push & pull buff: %c, %c, %c\n", primero, segundo, tercero);
	
	/* read & write msg */
	
	char fn[] = "/tmp/wendy";
	int rfd, wfd;
	if (mkfifo(fn, S_IRWXU) != 0)
		perror("Crear fifo");
    if ((rfd = open(fn, O_RDONLY)) < 0)
		perror("Abrir read-end");
	if ((wfd = open(fn, O_WRONLY)) < 0)
		perror("Abrir write-end");
		
	struct t_msg mensaje;
	
	int i = 0;
	
	while (i < 3) {
	
		read_msg(rfd, &prueba, &mensaje);
		
		char mes[BUFF_SIZE];
		sprint_msg(mensaje, &mes[0]);
		printf("mensaje recibido: %s\n", mes);
	
		i++;
		
		int pid = parse_bytestoint(mensaje.msg, 0);
		
		struct t_client cliente = create_client(pid);
		printf(" created client with pid: %d (%x)\n", pid, pid);
		
		sleep(1);
		
		struct t_msg respuesta;
		respuesta.cid = 0xff;
		respuesta.type = 0xf1;
		respuesta.size = 5;
		respuesta.msg[0] = cliente.cid;
		parse_inttobytes(pid, &respuesta.msg[1]);
		
		char cfn[20];
		sprintf(cfn, "/tmp/i_%d", pid);
		int fifo_client = open(cfn ,O_RDWR);
		if(fifo_client < 0)
			perror("Fifo Client");
		
		send_msg(fifo_client, respuesta); 
		
		sleep(3);
		
		close(fifo_client);
	}
	
	close(wfd);
	close(rfd);
	unlink(fn);
}
