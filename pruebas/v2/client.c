/* CLIENT */
/*
 * Este programa envia un mensaje de Handshake por el pipe wendy.
 * 
 */
#define _OPEN_SYS
#include<stdio.h> 
#include<fcntl.h>
#include<stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <string.h>


int main()
{
	int fifo_wendy;
	int result;
	
	pid_t pid;
	pid = getpid();
	
	printf("Porcess id: %d (0x%x)\n", pid, pid);
	
	char fn[20];
	int rfd, wfd;
	sprintf(fn, "/tmp/i_%d", pid);
	
	// crear FIFO
	if (mkfifo(fn, S_IRWXU) == 0) {
		
		printf("Pipe %s created.\n",fn);

		char msg[] = { 0x00, 0xf0, 0x04, 
			(pid >> 24) & 0xff, 
			(pid >> 16) & 0xff, 
			(pid >> 8) & 0xff, 
			pid & 0xff, 0x37 };
		
		fifo_wendy=open("/tmp/wendy",O_RDWR);
		if(fifo_wendy < 0) {
			printf("Error in opening wendy");
			exit(-1);
		}

		write(fifo_wendy, &msg, sizeof(msg));
		//write(fifo_wendy, &msg, sizeof(msg));
		//write(fifo_wendy, &msg, sizeof(msg));
		
		printf("Handshake sent to wendy.\n");
			
		close(fifo_wendy);

		// abrir read end en el FIFO
		if ((rfd = open(fn, O_RDONLY )) >= 0) {

			// abrir write end en el FIFO
			// Al parecer es necesario para evitar que entren mensajes
			// aleatorios e infinitos....  ??
			if ((wfd = open(fn, O_WRONLY)) >= 0) {
				
				char in[64];
				
				// Expect response...
				if (read(rfd, in, sizeof(in)) >= 0) {
		
					int cid; //client id
					
					printf("Received message from server\n");
					
					cid = (char) in[3];
					
					printf("Handshake succesful..\nClient id: %02x\n",(unsigned char) cid);
					
				}
							
				close(wfd);
			}
			else
				perror("open() error for write end");

			close(rfd);
		}
		else
			perror("open() error for read end");

		unlink(fn);
	}
	else 
		perror("mkfifo() error");
    
	return 0;
}
