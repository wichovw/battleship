#include<stdio.h> 
#include<fcntl.h>
#include<stdlib.h>
#define _OPEN_SYS
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

void *wendy(void *params);

main() {
	int fifo_wendy;
	
	pthread_t tid;
	pthread_attr_t attr;
	
	fifo_wendy = mkfifo("/tmp/wendy", 0666); 
	
	if(fifo_wendy<0) {
	 printf("Unable to create a fifo\n");
	 exit(-1);
	 }
	 
	printf("fifo wendy created successfuly\n");
	
	/*while(1) {
		
		pthread_create(&tid, &attr, wendy, NULL);
		
		printf("New thread created ready for Wendy...\n");
		
	}*/
	 
	pthread_create(&tid, &attr, wendy, NULL);
	
	printf("Wendy's thread created succesfully\n");
		
	while(1) {
	
		char msg[20];
		printf("\nEscribir 'exit' para salir...\n");
		scanf ("%s", msg);
		
		// saber como se deberia comparar esto en realidad...
		if (msg[0] == 'e' && msg[1] == 'x' && msg[2] == 'i' && msg[3] == 't')
			break;
	}
	
	pthread_cancel(tid);
	
	printf("Wendy's thread terminated on main\n");
	
	unlink("/tmp/wendy");
}

void *wendy(void *params) {
	
	int fifo_wendy;
	char in[64];
	
	fifo_wendy = open("/tmp/wendy",O_RDWR);
	
	if(fifo_wendy<1) {
	 printf("Error opening file\n");
	 }
	 
	printf("Fifo wendy opened succesfully...\n");
		
	while (read(fifo_wendy, &in, sizeof(in)) >= 0) {
		
		printf("\nReceived message from Wendy\n");
		
		printf("id: %d\n", in[0]);
	}
	
	printf("Wendy's thread terminated on thread\n");
	
	pthread_exit(0);
}
