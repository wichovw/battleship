
#define _OPEN_SYS
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>


int main() {
	char fn[] = "/tmp/wendy";
	int wfd;
	
	char message[] = { 0x00, 0xF0, 0x04, 0x00, 0x00, 0x00, 0xA0, 0x44 };
	
	if ((wfd = open(fn, O_WRONLY)) >= 0) {
		
		write(wfd, message, strlen(message));
		write(wfd, message, strlen(message));
		write(wfd, message, strlen(message));
		
		close(wfd);
	}
	
	else
		perror("open() error for write end");
		
	return 0;
}
