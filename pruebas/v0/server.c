/* SERVER */
/*
 * Este programa crea un thread para recibir a nuevos clientes.
 * Crea un nuevo thread para cada nuevo cliente que recibe.
 * 
 * despues del build hay que correr el comando en la terminal:
 * $ gcc -pthread -o server server.c
 * 
 */
#define _OPEN_SYS
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>

char fn[] = "/tmp/wendy";
int rfd, wfd;
void *wendy();
void *mgr(void *params);
int checksum(char *msg);

int main() {
	
  pthread_t tid;
  pthread_attr_t attr;
  
  pthread_attr_init(&attr);
  
  // crear FIFO
  if (mkfifo(fn, S_IRWXU) == 0) {
	  
	// abrir read end en el FIFO
    if ((rfd = open(fn, O_RDONLY /*| O_NONBLOCK*/ )) >= 0) {
		
	  // abrir write end en el FIFO
	  // Al parecer es necesario para evitar que entren mensajes
	  // aleatorios e infinitos....  ??
      if ((wfd = open(fn, O_WRONLY)) >= 0) {
		
		pthread_create(&tid, &attr, wendy, NULL);
		
		while(1) {
		
			char msg[20];
			printf("\nEscribir 'exit' para salir...\n");
			scanf ("%s", msg);
			
			// saber como se deberia comparar esto en realidad...
			if (msg[0] == 'e' && msg[1] == 'x' && msg[2] == 'i' && msg[3] == 't')
				break;
		}
		
		pthread_cancel(tid);
		
        close(wfd);
      }
      else
        perror("open() error for write end");
        
      close(rfd);
    }
    else
      perror("open() error for read end");
      
    unlink(fn);
  }
  else 
    perror("mkfifo() error");
    
  return 0;
}

void *wendy() {
	  
	char in[64];
		
	while (read(rfd, in, sizeof(in)) >= 0) {
		
		unsigned int pid;
		
		printf("Received message from Wendy\n");
		
		if (checksum(in) != 0){
			printf("Checksum of message failed\n");
			printf("Message won't be processed\n");
			continue;
		}
		
		pid = ((unsigned char) in[3] << 24) | 
			((unsigned char) in[4] << 16) | 
			((unsigned char) in[5] << 8) | 
			(unsigned char) in[6];
		
		printf("New client pid: %d (%02x %02x %02x %02x)\n", (unsigned int) pid,
			(unsigned char) in[3],
			(unsigned char) in[4],
			(unsigned char) in[5],
			(unsigned char) in[6]);
			
		pthread_t tid;
		pthread_attr_t attr;
		
		pthread_attr_init(&attr);
		
		pthread_create(&tid, &attr, mgr, &pid);
		
	}
	
	pthread_exit(0);
}

void *mgr(void *params)
{
	int fifo_client;
	int *pidB = (int *) params;
	int pid = *pidB;
	char fi[20], fo[20];
	sprintf(fo, "/tmp/i_%d", pid);
	sprintf(fi, "/tmp/o_%d", pid);
	
	printf("Mannager thread created for client %d\n", pid);
	
	// Handshake response	
	char msg[] = { 0xff, 0xf1, 5, 1,
		(pid >> 24) & 0xff, 
		(pid >> 16) & 0xff, 
		(pid >> 8) & 0xff, 
		pid & 0xff, 55 };
	
	fifo_client = open(fo ,O_RDWR);
	if(fifo_client < 0) {
		printf("Error in opening file");
		pthread_exit(0);
	}

	write(fifo_client, &msg, sizeof(msg));
	
	printf("Handshake response sent to client %d.\n", pid);

	close(fifo_client);
	
	return 0;
}

int checksum(char *msg){
	return 0;
}
/*
char *buffer;
int buff = 0;

int read_msg(int cid, char *msg[]){
	
	int pipe;
	pipe = getPipe(cid, 'r');
	
	char m[64];
	int i = 0;
	while (buff > 0){
		m[i] = *buffer[i];
	}
	
	if(read(pipe, m, sizeof(m)) >= 0){
		
	}
	else
		return -1;
}

int getPipe(int cid, char end){
	if (end == 'r')
		return rfd;
	if (end == 'w')
		return wfd;
}
*/
