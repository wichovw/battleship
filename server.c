/* SERVER */

#define _OPEN_SYS
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <stdlib.h>

#define CLNTS 20 // Numero maximo de clientes
#define BUFF_SIZE 512 // tamano de buffer de mensajes
#define DEBUG_MODE 1

struct t_client {
  int pid, rfd, wfd, i, swfd, jplayer;
  unsigned char cid;
  struct t_juego *juego;
};

struct t_buffer {
  char buff[BUFF_SIZE];
  int pos;
};

struct t_msg {
  unsigned char cid, type, size, chk;
  unsigned char msg[256];
};

struct j_barco_plan {
	unsigned char tamano, cantidad;
};

struct t_juego {
	struct t_client *jug1, *jug2;
	int size, live, i, barcos_num;
	char nombre[BUFF_SIZE];
	unsigned char jid;
	struct j_barco_plan barcos[BUFF_SIZE];
	char tablero[2][255][255];
};

struct t_client clist[CLNTS];
char next_cid = 0xC0;
int active_clients = 0;

struct t_juego jlist[CLNTS];
char next_jid = 0xB0;
int active_games = 0;

void wait_some_clients();
int respond_to_client(struct t_client *clnt, struct t_msg msg);
int respond_to_server();
struct t_juego *create_game(struct t_client *client, int size, char *nombre, int nombre_size, unsigned char *barcos, int barcos_size);

int main() {
	printf("Se ha iniciado el Battleship Server...\n");
	wait_some_clients();
	exit(0);
}

/* CAPA DE COMUNICACION */

void concat(char *arr1, int size1, char *arr2, int size2, char *result, int printable) {
	
	int i = 0;
	
	while (i < size1) {
		if (*arr1 == 0 && printable == 1) break;
		*result = *arr1;
		result++;
		arr1++;
		i++;
	}
	
	i = 0;
	while (i < size2) {
		*result = *arr2;
		result++;
		arr2++;
		i++;
	}
}

int push_buff(struct t_buffer *buffer, unsigned char in) {
	
	if ((*buffer).pos < sizeof((*buffer).buff)) {
		
		(*buffer).buff[(*buffer).pos] = in;
		(*buffer).pos++;
		return 0;
	}
	else return -1;
}

int pull_buff(struct t_buffer *buffer, unsigned char *out) {
	
	if ((*buffer).pos > 0) {
		
		*out = (*buffer).buff[0];
		int i = 0;
		while (i < (*buffer).pos - 1) {
			(*buffer).buff[i] = (*buffer).buff[i + 1];
			i++;
		}
		(*buffer).pos--;
		return 0;
	}
	else return -1;
}

int read_package(int read_end, struct t_buffer *buffer) {
	
	char in[BUFF_SIZE];
	
	int nb;
	nb = read(read_end, in, sizeof(in));
	
	//printf("Received package size: %d\n",nb);
	
	if (nb >= 0) {
		
		int i = 0;
		while (i < nb) {
			
			if (push_buff(buffer, in[i]) < 0) break;
			i++;
		}
		return 0;
	}
	else return -1;
}

void sprint_msg(struct t_msg msg, char *str) {
	
	char s1[10];
	
	sprintf((char *)str, "%02x %02x %02x ", 
		(unsigned char) msg.cid, 
		(unsigned char) msg.type, 
		(unsigned char) msg.size);
		
	int i = 0;
	while (i < msg.size) {
		sprintf(s1, "%02x ", (unsigned char) msg.msg[i]);
		concat((char *)str, BUFF_SIZE, &s1[0], 10, (char *)str, 1);
		i++;
	}
	sprintf(s1, "%02x", (unsigned char) msg.chk);
	concat(str, BUFF_SIZE, &s1[0], 3, str, 1);
}

int checksum(struct t_msg msg) {
	if (msg.chk == 0x37)
		return 0;
	else return -1;
}

char calc_checksum(struct t_msg msg) {
	return 0x37;
}

int pre_read_msg(int read_end, struct t_buffer *buffer, struct t_msg *msg) {
	
	if (pull_buff(buffer, &(*msg).cid) < 0) return -1;
	if (pull_buff(buffer, &(*msg).type) < 0) return -1;
	if (pull_buff(buffer, &(*msg).size) < 0) return -1;
	
	int i = 0;
	while (i < (*msg).size) {
		if (pull_buff(buffer, &(*msg).msg[i]) < 0) return -1;
		i++;
	}
	
	if (pull_buff(buffer, &(*msg).chk) < 0) return -1;
	
	if (checksum(*msg) < 0) return -2;
	
	return 0;
}

void read_msg(int read_end, struct t_buffer *buffer, struct t_msg *message) {
	while(1) {
		int pr = pre_read_msg(read_end, buffer, message);
		
		if (pr < 0) {
			if (pr == -1) {
				read_package(read_end, buffer);
			}
			else if (pr == -2) {
				printf("Se ha corrompido algun mensaje entrante. (Checksum err)\n");
				unsigned char a;
				pull_buff(buffer, &a);
			}
		}
		else break;
	}
	
	if (DEBUG_MODE > 0) {
		char mes [BUFF_SIZE];
		sprint_msg(*message, &mes[0]);
		printf("     $debug msg read: %s\n", mes);
	}
}

int parse_bytestoint(unsigned char *input, int offset) {
	
	int r = ((unsigned char) *(input + offset)     << 24) |
	        ((unsigned char) *(input + offset + 1) << 16) |
	        ((unsigned char) *(input + offset + 2) << 8 ) |
	         (unsigned char) *(input + offset + 3);
	         
	return r;
}

void parse_inttobytes(int input, unsigned char *value) {
	
	int i = 3;
	
	while (i >= 0) {
		*value = (input >> i * 8) & 0xff;
		value++;
		i--;
	}
}

void send_msg(int write_end, struct t_msg msg) {
	
	char m[msg.size + 4];
	
	m[0] = msg.cid;
	m[1] = msg.type;
	m[2] = msg.size;
	
	int i = 0;
	while (i < msg.size) {
		m[i + 3] = msg.msg[i];
		i++;
	}
	m[i + 3] = calc_checksum(msg);
	
	write(write_end, &m, sizeof(m)); 
	
	if(DEBUG_MODE > 0) {
		char mes [BUFF_SIZE];
		sprint_msg(msg, &mes[0]);
		printf("     $debug msg sent: %s\n", mes);
	}
} 

/* PROTOCOLO */

// Handshake response
void send_hsr(int write_end, char cid, int pid) { 
	
	struct t_msg hsr;
	hsr.cid = 0xff;
	hsr.type = 0xf1;
	hsr.size = 5;
	hsr.msg[0] = cid;
	parse_inttobytes(pid, &hsr.msg[1]);
	
	send_msg(write_end, hsr);
}

// Server state response
void server_state(int write_end) {
	
	int waitin, playin, tot;
	waitin = 0;
	playin = 0;
	tot = 0;
	
	while(tot < active_games) {
		if (jlist[tot].live > 0)
			playin++;
		else
			waitin++;
		tot++;
	}
	
	struct t_msg ss;
	ss.cid = 0xff;
	ss.type = 0xf3;
	ss.size = 2;
	ss.msg[0] = (unsigned char) playin;
	ss.msg[1] = (unsigned char) waitin;
	
	send_msg(write_end, ss);
}

// Create game request
void create_game_req(struct t_client *client, struct t_msg msg) {
	
	unsigned char size = msg.msg[0];
	
	char nombre[255];
	unsigned char *p = &msg.msg[1];
	int i = 0;
	while(i < 255) {
		nombre[i] = *p;
		if(*p == 0)
			break;
		p++;
		i++;
	}
	
	unsigned char *barcos = &msg.msg[1];
	i = 0;
	while(i < BUFF_SIZE) {
		if (*barcos == 0)
			break;
		barcos++;
		i++;
	}
	barcos++;
	int nombre_size = i + 1;
	int barcos_size = msg.size - nombre_size;
	
	struct t_juego *juego;
	juego = create_game(client, size, &nombre[0], nombre_size, barcos, barcos_size);
	
	struct t_msg ccg;
	ccg.cid = 0xff;
	ccg.type = 0xfb;
	ccg.size = 1;
	ccg.msg[0] = (*juego).jid;
	
	send_msg((*client).wfd, ccg);
	
	(*client).juego = juego;
	(*client).jplayer = 0;
}

// Play Game List request
void playlist_req(int write_end) {
	
	struct t_msg pl;
	pl.cid = 0xff;
	pl.type = 0xf5;
	
	int i = 0;
	int j = 0;
	while (j < active_games) {
		pl.msg[i++] = jlist[j].jid;
		pl.msg[i++] = jlist[j].size;
		int c = 0;
		char *p = &jlist[j].nombre[0];
		while(c++ < sizeof(jlist[j].nombre)) {
			pl.msg[i++] = *p;
			if(*p == 0)
				break;
			p++;
		}
	j++;
	}
	pl.size = i;
	
	send_msg(write_end, pl);
}

// Play Game request
void playgame_req(struct t_client *client, unsigned char jid) {
	
	int i = 0;
	struct t_juego *juego;
	while(i < active_games) {
		juego = &jlist[i];
		if((*juego).jid == jid)
			break;
		i++;
	}
	
	struct t_msg cpg;
	cpg.cid = 0xff;
	cpg.type = 0xf9;
	
	if(i >= active_games || (*juego).live > 0) {
		// no se pudo unir al juego
		cpg.size = 0;
	}
	else {
		cpg.size = 1 + ((*juego).barcos_num * 2);
		cpg.msg[0] = (*juego).size;
		int b = 0;
		unsigned char *p = &cpg.msg[1];
		while (b < (*juego).barcos_num) {
			*(p++) = (*juego).barcos[b].cantidad;
			*(p++) = (*juego).barcos[b++].tamano;
		}
		(*juego).jug2 = client;
	}
	send_msg((*client).wfd, cpg);
	
	struct t_msg sg;
	sg.cid = 0xff;
	sg.type = 0x01;
	sg.size = 0;
	
	send_msg((*(*juego).jug1).wfd, sg);
	
	(*client).juego = juego;
	(*client).jplayer = 1;
}

/* CAPA DE LO QUE SEA ENTRE COMUNICACION Y APLICACION */

struct t_client create_client(int pid) {
	
	// Se bloquea hasta que haya un espacio
	if (active_clients >= CLNTS)
		printf("     El servidor se encuentra en su maxima capacidad.\n     El cliente sera atendido cuando se libere un espacio...\n");
	while(active_clients >= CLNTS) ;
	
	clist[active_clients].cid = next_cid;
	clist[active_clients].pid = pid;
	clist[active_clients].i = active_clients;
	
	active_clients++;
	next_cid++;
	
	return clist[active_clients - 1];
}

void free_client(int cindex) {
	
	while(cindex + 1 < CLNTS){
		
		clist[cindex] = clist[cindex + 1];
		clist[cindex].i--;
		cindex++;
	}
	
	active_clients--;
}

void close_cfifos() {
	
	int ci = 0;
	while(ci < active_clients) {
		
		struct t_client clnt = clist[ci];
		
		char fn[20];
		sprintf(fn, "/tmp/o_%d", clnt.pid);
		unlink(fn);
		
		char sfn[20];
		sprintf(sfn, "/tmp/sm_%02x", clnt.cid);
		unlink(sfn);
		
		ci++;
	}
}

void *mgr(void *params) {
	
	int pid = *((int *) params);
	
	struct t_client client = create_client(pid);
	printf("[%02x] Manager initialized. (0x%x)\n", client.cid, pid);
	
	char fnin[20], fnout[20];
	sprintf(fnin, "/tmp/i_%d", pid);
	sprintf(fnout, "/tmp/o_%d", pid);
	char fnsm[] = "/tmp/server_master";
	
	int wfd, rfd, swfd, rfdw;
	if ((wfd = open(fnin, O_WRONLY)) < 0)
		perror("Abrir write-end client i_");
	if ((swfd = open(fnsm, O_WRONLY)) < 0)
		perror("Abrir write-end server master");
	
	if (mkfifo(fnout, S_IRWXU) != 0)
		perror("create input fifo o_");
		
	send_hsr(wfd, client.cid, pid);
	
	if ((rfd = open(fnout, O_RDONLY)) < 0)
		perror("Abrir read-end o_");
	if ((rfdw = open(fnout, O_WRONLY)) < 0)
		perror("Abrir write-end o_");
	
	client.wfd = wfd;
	client.rfd = rfd;
	
	struct t_buffer rbuff;
	rbuff.pos = 0;
	
	struct t_msg rcv;
	char mes[BUFF_SIZE];
	
	while(1) {
		
		read_msg(rfd, &rbuff, &rcv);
		
		sprint_msg(rcv, &mes[0]);
		// printf("[%02x] Rcvd msg: %s\n", client.cid, mes);
		
		if(rcv.cid == 0xff) {
			respond_to_server();
			break;
		}
		
		if(respond_to_client(&client, rcv) < 0)
			break;
	}
	
	sleep(1);
	
	printf("[%02x] Comunication with client terminated.\n", client.cid);
	free_client(client.i);
	
	close(wfd);
	close(rfd);
	close(rfdw);
	pthread_exit(0);
}

void *wendy(void *params) {
		
	int rfd = *((int *) params);
	
	struct t_msg hs;
	struct t_buffer wbuff;
	wbuff.pos = 0;
	char mes[BUFF_SIZE];
	
	while(1) {
		
		read_msg(rfd, &wbuff, &hs);
		
		sprint_msg(hs, &mes[0]);
		printf("[wd] Rcvd msg: %s\n", mes);
		
		if((unsigned char) hs.type != 0xf0) {
			printf("[wd] Expected msg type to be Handshake (0xf0). Msg ignored.\n");
			continue;
		}
		
		int pid = parse_bytestoint(hs.msg, 0);
		
		pthread_t tid;
		pthread_attr_t attr;
		
		pthread_attr_init(&attr);
		
		pthread_create(&tid, &attr, mgr, &pid);
	}
	
	pthread_exit(0);
}

void *server_master(void *params) {
	
	printf("[sm] Server Master initialized...\n");
	
	int rfd = *((int *) params);
	
	struct t_buffer rbuff;
	rbuff.pos = 0;
	
	struct t_msg rcv;
	char mes[BUFF_SIZE];
	
	while(1) {
		
		read_msg(rfd, &rbuff, &rcv);
		
		sprint_msg(rcv, &mes[0]);
		printf("[sm] Rcvd msg: %s\n", mes);
	}
}

void wait_some_clients() {
	
	char fn[] = "/tmp/wendy";
	int rfd, wfd;
	if (mkfifo(fn, S_IRWXU) != 0)
		perror("Crear fifo wendy");
	if ((wfd = open(fn, O_WRONLY)) < 0)
		perror("Abrir write-end wendy");
    if ((rfd = open(fn, O_RDONLY)) < 0)
		perror("Abrir read-end wendy");
		
	char sfn[] = "/tmp/server_master";
	int srfd, swfd;
	if (mkfifo(sfn, S_IRWXU) != 0)
		perror("Crear fifo server master");
		
	pthread_t wtid;
	pthread_attr_t wattr;
	
	pthread_attr_init(&wattr);
	pthread_create(&wtid, &wattr, wendy, &rfd);
	
    if ((srfd = open(sfn, O_RDONLY)) < 0)
		perror("Abrir read-end server master");
	if ((swfd = open(sfn, O_WRONLY)) < 0)
		perror("Abrir write-end server master");
	
	pthread_t stid;
	pthread_attr_t sattr;
	
	pthread_attr_init(&sattr);
	pthread_create(&stid, &sattr, server_master, &srfd);
	
	while(1) {
		char msg[20];
		printf("Escribir 'exit' para salir...\n");
		scanf ("%s", msg);
		// saber como se deberia comparar esto en realidad...
		if (msg[0] == 'e' && msg[1] == 'x' && msg[2] == 'i' && msg[3] == 't')
			break;
	}
	
	printf("Cerrando el server...\n");
	
	close_cfifos();
	close(rfd);
	close(wfd);
	close(srfd);
	close(swfd);
	unlink(fn);
	unlink(sfn);
}

/* CAPA DE APLICACION */

void place_ships(struct t_client clnt, struct t_msg msg) {
	
	struct t_juego *juego = clnt.juego;
	
	int c = 0;
	while (c < (*juego).size) {
		int f = 0;
		while (f < (*juego).size) {
			(*juego).tablero[clnt.jplayer][c][f++] = '~';
		}
		c++;
	}
	
	int i = 0;
	unsigned char *p = &msg.msg[0];
	while(i < msg.size) {
		unsigned char xi = *(p++);
		unsigned char yi = *(p++);
		unsigned char xf = *(p++);
		unsigned char yf = *(p++);
		
		int tamano = xf - xi;
		char dir = 'h';
		
		if (tamano == 0){
			tamano = yf - yi;
			dir = 'v';
		}
		
		int x = xi;
		int y = yi;
		int bt = 0; 
		while(bt++ < tamano){
			(*juego).tablero[clnt.jplayer][y][x] = '#';
			if(dir == 'h')
				x++;
			else
				y++;
		}
		
		i += 4;
	}
	
	struct t_msg ft;
	ft.cid = 0xff;
	ft.type = 0x03;
	ft.size = 0;
	
	printf("[%02x] El jugador %d esta listo para empezar a jugar!\n", clnt.cid, clnt.jplayer);
	if ((*juego).live > 0) {
		printf("     El juego %02x ha comenzado!\n", (*juego).jid);
		send_msg((*(*juego).jug1).wfd, ft);
	}
	else
	{
		(*juego).live = 1;
	}
}

struct t_juego *create_game(struct t_client *client, int size, char *nombre, int nombre_size, unsigned char *barcos, int barcos_size) {
	
	while(active_games >= CLNTS) ;
	
	jlist[active_games].i = active_games;
	jlist[active_games].jid = next_jid;
	jlist[active_games].size = size;
	concat(nombre, nombre_size, nombre, 0, &(jlist[active_games].nombre[0]), 1);
	
	int i = 0;
	while(barcos_size > 0){
		struct j_barco_plan barco;
		barco.cantidad = *barcos;
		barco.tamano = *(barcos + 1);
		barcos += 2;
		barcos_size -= 2;
		jlist[active_games].barcos[i] = barco;
		i++;
	}
	jlist[active_games].barcos_num = i - 1;
	
	jlist[active_games].jug1 = client;
	jlist[active_games].live = 0;
	
	printf("[%02x] New game %02x created.\n", (*client).cid, jlist[active_games].jid);
	printf("     Name: '%s' Board size: %d\n", jlist[active_games].nombre, jlist[active_games].size);
	
	active_games++;
	next_jid++;
	
	return &jlist[active_games - 1];
}

int respond_to_server() {
	return 0;
}

int respond_to_client(struct t_client *clnt, struct t_msg msg) {
	
	printf("[%02x] ", (*clnt).cid);
	switch(msg.type) {
		
		case 0xf2:
		printf("Server state requested...\n");
		server_state((*clnt).wfd);
		break;
		
		case 0xfa:
		printf("Create new game requested...\n");
		create_game_req(clnt, msg);
		break;
		
		case 0xf4:
		printf("Play game list requested...\n");
		playlist_req((*clnt).wfd);
		break;
		
		case 0xf8:
		printf("Requested to play in game...\n");
		playgame_req(clnt, msg.msg[0]);
		break;
		
		case 0x02:
		printf("Colocar barcos en tablero...\n");
		place_ships(*clnt, msg);
		break;
		
		// mensaje que no es del protocolo para salir
		case 0xee:
		printf("Client disconected...\n");
		return -2;
		
		default:
		printf("Msg type not recognized. (%02x)\n", msg.type);
		break;
	}
	
	return 0;
}
